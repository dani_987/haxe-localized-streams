# Haxe Typed Json Parser

Streams, that know the Source and can report some errors during parsing

## Install

```
haxelib install localized-streams
```

## Usage

### Creating Streams

Creating a Stream from a File
```
var fileStream : hx.streams.LocalizedInput = hx.streams.FileInput(path_to_file);
```
Creating a Stream from a String
```
var stringStream : hx.streams.LocalizedInput = hx.streams.StringInput("Some random Text", "TheSourceOfTheText");
```
Creating a Stream from an `haxe.io.Input`
```
var stringStream : hx.streams.LocalizedInput = hx.streams.LocalizedHaxeInput(haxeStream, "TheSourceOfTheStream");
```

### Reading from Streams
```
var stringStream : hx.streams.LocalizedInput = hx.streams.StringInput("ABCDEF");

//Only read, but do not move the cursor
trace(stringStream.previewNextChars(1)); // Traces: "A"
trace(stringStream.previewNextChars(3)); // Traces: "ABC"
trace(stringStream.previewNextChars(2)); // Traces: "AB"

//Read and move the cursor
trace(stringStream.getNextChars(1)); // Traces: "A"

//Check for EOF
trace(stringStream.isEof()); // Traces: false

//Only read, but do not move the cursor
trace(stringStream.previewNextChars(1)); // Traces: "B"
trace(stringStream.previewNextChars(2)); // Traces: "BC"

//Read and move the cursor
trace(stringStream.getNextChars(2)); // Traces: "BC"
trace(stringStream.getNextChars(3)); // Traces: "DEF"

//Check for EOF (EOF was not read, because we haven't read after the last sign)
trace(stringStream.isEof()); // Traces: false

//EOF is reached
trace(stringStream.previewNextChars(1)); // Traces: ""
trace(stringStream.isEof()); // Traces: true
trace(stringStream.getNextChars(1)); // Traces: ""
```

### Indicating Errors in Streams

```
var fileStream : hx.streams.LocalizedInput = hx.streams.FileInput(path_to_file);
var errorFollower = new ErrorDetailFollower(fileStream.getSourceName());
var readString : String;

//Reading without moving the cursor
readString = fileStream.previewNextChars(1);

//Reading and moving the cursor: You need to Update the errorFollower as well!
readString = stringStream.getNextChars(1);
errorFollower.updateRead(readString);

//More reading ...


//Reporting an error:
var errorData = follower.createError("The detailed Error-Message", fileStream);
```

### Using the ErrorData

Tracing error to Console:
```
ErrorDetailFollower.trace(error);
```

Gives an output like this:
```
###############
ERROR in TestCase @2,5: SampleErrorMessage
    1 | aaAAAaaAAA
    2 | bbBBcbbBBB
----- +     ^
###############
```

Getting the Error as an Array:
```
trace(ErrorDetailFollower.traceToArray(error));
```

Returns an Array with the same Content:
```
[###############,ERROR in TestCase @2,5: SampleErrorMessage,    1 | aaAAAaaAAA,    2 | bbBBcbbBBB,----- +     ^,###############]
```
