package test.streams;

import haxe.io.Bytes;
import hx.streams.StringOutput;
import buddy.BuddySuite;

using buddy.Should;

class WritingToStreamWorks extends BuddySuite {

    public function new() {
        describe("The StringOutput", {
            it("should create correct Strings", {
                var output = new StringOutput();
                output.writeString("Some Test");
                output.writeBytes(Bytes.ofString("Some Data"));
                output.finish();
                output.getString().should.be("Some TestSome Data");
            });
        });
    }
}
