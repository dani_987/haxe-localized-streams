package test.streams;
import haxe.io.Bytes;
import hx.streams.StringInput;
import hx.streams.LocalizedInput;
import buddy.BuddySuite;

using buddy.Should;

class ReadingFromStreamsWorksCorrect extends BuddySuite {

    public function new() {
        describe("The StringInput", {
            var inputString = "ABCDEF";

            var input : LocalizedInput = new StringInput(Bytes.ofString(inputString));
            it("should allow reading without moving the cursor", {
                input.previewNextChars(1).should.be("A");
                input.previewNextChars(3).should.be("ABC");
                input.previewNextChars(1).should.be("A");
            });
            it("should indicate, the EOF is not reached", {
                input.isEof().should.be(false);
            });
            it("should allow reading with moving the cursor", {
                input.getNextChars(1).should.be("A");
                input.getNextChars(2).should.be("BC");
                input.getNextChars(3).should.be("DEF");
            });
            it("should return an empty String on EOF", {
                input.previewNextChars(5).should.be("");
                input.getNextChars(5).should.be("");
            });
            it("should indicate, the EOF is reached", {
                input.isEof().should.be(true);
            });
        });
    }
}
