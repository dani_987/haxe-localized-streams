package test.string;
import buddy.BuddySuite;

using buddy.Should;
using hx.string.StringReaderTools;

class StringReaderToolsWorks extends BuddySuite {

    public function new() {
        describe("StringReaderTools", {
            it("should extract a Hex-String to Number", {
                "0F".hexToInt64().should.be(0x0F);
                "aF34".hexToInt64().should.be(0xAF34);
            });
        });
    }
}
