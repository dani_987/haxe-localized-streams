package test.string;
import haxe.io.Bytes;
import haxe.Timer;
import hx.string.StringBuilder;
import buddy.BuddySuite;

using buddy.Should;

class UsingTheStringBuilderWorks extends BuddySuite {

    public function new() {
        describe("The StringBuilder", {
            it("should add multiple Strings together", {
                var builder = new StringBuilder();
                builder.add("Hallo").add(" ").addBytes(Bytes.ofString("Welt")).addBuilderData(
                    new StringBuilder().add("!").add("!!")
                );
                builder.toString().should.be("Hallo Welt!!!");
            });
            it("should add multiple Strings really fast", {
                var builder = new StringBuilder();
                var compareString = "";
                function addMuchToString(addFunction: (String)->Void) {
                    for(loop in 0...500){
                        for(charCode in "a".code..."z".code){
                            addFunction(String.fromCharCode(charCode));
                        }
                    }
                }
                var start = Timer.stamp();
                addMuchToString(builder.add);
                var builderResult = builder.toString();
                var builderTime = Timer.stamp() - start;

                start = Timer.stamp();
                addMuchToString((toAdd)->{compareString += toAdd;});
                var stringTime = Timer.stamp() - start;

                builderResult.should.be(compareString);
                (builderTime*2).should.beLessThan(stringTime);
            });
        });
    }
}
