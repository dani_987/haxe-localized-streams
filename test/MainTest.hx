package test;
using buddy.Should;

class MainTest implements buddy.Buddy<[
    test.streams.ReadingFromStreamsWorksCorrect,
    test.streams.ErrorDetailWorksProperly,
    test.streams.WritingToStreamWorks,
    test.string.UsingTheStringBuilderWorks,
    test.string.StringReaderToolsWorks,
]>{}
