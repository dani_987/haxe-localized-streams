package hx.streams;

import haxe.io.Bytes;

interface LocalizedOutput {
    public function getDestinationName() : String;
    public function writeString(out: String): Bool;
    public function writeBytes(out: Bytes): Bool;
    public function finish(): Void;
}
