package hx.streams;

import haxe.io.Bytes;
import hx.string.StringBuilder;

class StringOutput implements LocalizedOutput {
    private var isOpen : Bool;
    private var destination: String;
    private var data : StringBuilder;
    public function new(destName: String = "<Unknown String-Desination>") {
        this.isOpen = true;
        this.destination = destName;
        this.data = new StringBuilder();
    }

    public function getDestinationName():String { return destination; }

    public function writeString(out:String):Bool {
        if(!isOpen)return false;
        data.add(out);
        return true;
    }

    public function writeBytes(out:Bytes):Bool {
        if(!isOpen)return false;
        data.addBytes(out);
        return true;
    }

    public function finish() { isOpen = false; }

    public function getString() : String {
        return data.toString();
    }

    public function getBytes() : Bytes {
        return data.getBytes();
    }
}
