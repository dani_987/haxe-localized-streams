package hx.streams;

import haxe.io.Bytes;
import hx.streams.ErrorDetail.ErrorDetailFollower;

class PositionReader implements LocalizedInput {
    private var input: LocalizedInput;
    public var start: Int;
    public function new(ofInput: LocalizedInput) {
        this.input = ofInput;
        this.start = 0;
    }

    public function clone() : PositionReader {
        var result = new PositionReader(this.input);
        result.moveStart(start);
        return result;
    }

    public function moveStart(difference: Int) {
        start += difference;
    }

    public function getError(message: String, contextLength: Null<Int> = null) : ErrorDetail {
        var follower : ErrorDetailFollower = null;
        if(contextLength == null) follower = new ErrorDetailFollower(input.getSourceName());
        else follower = new ErrorDetailFollower(input.getSourceName(), contextLength);
        var readString = input.previewNextChars(start);
        follower.updateRead(readString);
        return follower.createError(message, this);
    }

    public function getSourceName():String {return this.input.getSourceName();}

    public function getNextChars(length:Int):String {
        start += length;
        return input.previewNextBytes(start).getString(start - length, length);
    }

    public function previewNextChars(length:Int):String {
        var bytes = input.previewNextBytes(start + length);
        if(bytes.length<=start)return "";
        return bytes.getString(start, bytes.length-start);
    }

    public function isEof():Bool { return this.previewNextBytes(1).length == 0; }

    public function getNextBytes(length:Int):Bytes {
        var result = this.previewNextBytes(length);
        start += result.length;
        return result;
    }

    public function previewNextBytes(length:Int):Bytes {
        var read = input.previewNextBytes(start+length);
        var result = Bytes.alloc(read.length - start);
        result.blit(0, read, start, result.length);
        return result;
    }
}
