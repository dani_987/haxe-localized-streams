package hx.streams;

import haxe.io.BytesInput;
import haxe.io.Bytes;

class StringInput extends LocalizedHaxeInput {
    public function new(ofBytes: Bytes, sourceName: String = "<Unknown String-Source>") {
        super(new BytesInput(ofBytes), sourceName);
    }
}
