package hx.streams;

import haxe.io.Bytes;
import haxe.io.Output;

class LocalizedHaxeOutput implements LocalizedOutput {
    private var out : Output;
    private var isOpen : Bool;
    private var destination: String;
    public function new(output: Output, destName: String ) {
        this.out = output;
        this.destination = destName;
        this.isOpen = true;
    }

    public function getDestinationName():String { return destination; }

    public function writeString(out:String):Bool {
        if(!isOpen)return false;
        this.out.writeString(out);
        return true;
    }

    public function writeBytes(out:Bytes):Bool {
        if(!isOpen)return false;
        this.out.write(out);
        return true;
    }

    public function finish() {
        if(!isOpen)return;
        this.out.flush();
        this.out.close();
        isOpen = false;
    }
}
