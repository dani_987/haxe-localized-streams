package hx.streams;

import haxe.io.Bytes;

interface LocalizedInput {
    public function getSourceName() : String;
    public function getNextChars(length: Int) : String;
    public function previewNextChars(length: Int) : String;
    public function getNextBytes(length: Int) : Bytes;
    public function previewNextBytes(length: Int) : Bytes;
    public function isEof() : Bool;
}
