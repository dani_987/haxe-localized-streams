package hx.streams;

#if sys
import sys.io.File;

class FileOutput extends LocalizedHaxeOutput {
    public function new(fileName: String) {
        super(File.write(fileName), fileName);
    }
}

#end
