package hx.streams;

import haxe.io.BytesBuffer;
import haxe.io.Bytes;

abstract class LocalizedHaxeInput implements LocalizedInput {
    private var input:haxe.io.Input;
    private var dataBuffer: BytesBuffer;
    private var sourceName: String;
    private var readEof: Bool;
    private var requestedLastBytes: Bool;

    private function new(input:haxe.io.Input, sourceName: String) {
        dataBuffer = new BytesBuffer();
        readEof = false;
        requestedLastBytes = readEof;
        this.input = input;
        this.sourceName = sourceName;
    }


    public function getSourceName():String { return sourceName; }


    public function getNextChars(length:Int):String {
        return bytesToString(getNextBytes(length));
    }

    public function previewNextChars(length:Int):String {
        return bytesToString(previewNextBytes(length));

    }

    public function getNextBytes(length:Int):Bytes {
        var result = previewNextBytes(length);
        var oldLen = dataBuffer.length;
        if(result.length >= oldLen){
            requestedLastBytes = readEof;
            dataBuffer = new BytesBuffer();
        } else {
            var bytes = dataBuffer.getBytes();
            dataBuffer = new BytesBuffer();
            dataBuffer.addBytes(bytes, result.length, oldLen - result.length);
        }
        return result;
    }

    public function previewNextBytes(length:Int):Bytes {
        var availableLen = length;
        if(length > dataBuffer.length){
            var read = readBytes(length - dataBuffer.length);
            dataBuffer.addBytes(read.data, 0, read.read);
            availableLen = dataBuffer.length;
        }
        var result = Bytes.alloc(availableLen);
        if(availableLen > 0) result.blit(0, dataBuffer.getBytes(), 0, availableLen);
        return result;
    }

    public static function bytesToString(bytes: Bytes):String {
        #if neko
		return neko.Lib.stringReference(bytes);
		#else
		return bytes.getString(0, bytes.length);
		#end
    }

    private function readBytes(length:Int): {read: Int, data: Bytes} {
        var bytesBuffer = haxe.io.Bytes.alloc(length);
        var bytesToRead = length;
        var bytesRead = 0;
        try{
            while (bytesToRead > 0 && !readEof) {
                var bytesNowRead = input.readBytes(bytesBuffer, bytesRead, bytesToRead);
                if (bytesNowRead == 0)
                    readEof = true;
                bytesRead += bytesNowRead;
                bytesToRead -= bytesNowRead;
            }
        }catch (_: haxe.io.Eof) {readEof = true;}
		return {read: bytesRead, data: bytesBuffer};
    }

    public function isEof():Bool {
        return requestedLastBytes;
    }
}
