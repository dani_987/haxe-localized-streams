package hx.string;

import haxe.io.Bytes;

class StringBuilder {
    private var data : List<Bytes>;
    private var length : Int;
    public function new() {
        data = new List();
        length = 0;
    }

    public function add(toAdd: String) : StringBuilder {
        return this.addBytes(Bytes.ofString(toAdd));
    }

    public function addBytes(bytesToAdd: Bytes) : StringBuilder {
        length += bytesToAdd.length;
        data.add(bytesToAdd);
        return this;
    }

    public function addBuilderData(toAdd: StringBuilder) : StringBuilder {
        this.length += toAdd.length;
        for(bytesToAdd in toAdd.data)this.data.add(bytesToAdd);
        return this;
    }

    public function toString() : String {
        return getBytes().toString();
    }

    public function getBytes() : Bytes {
        var result = Bytes.alloc(length);
        var posInResult = 0;
        for(added in data){
            result.blit(posInResult, added, 0, added.length);
            posInResult+=added.length;
        }
        return result;
    }
}
