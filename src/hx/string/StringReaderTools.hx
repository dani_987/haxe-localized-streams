package hx.string;

import haxe.Int64;

class StringReaderTools {
    public static function hexToInt64(fromString: String) : Int64 {
        var result = 0;
        for(char in fromString.split("")){
            switch(char){
                case isBetween("0", _, "9") => num if (num >= 0) :
                    result = (result * 16) + num;
                case isBetween("a", _, "f") => hex if (hex >= 0) :
                    result = (result * 16) + hex + 10;
                case isBetween("A", _, "F") => hex if (hex >= 0) :
                    result = (result * 16) + hex + 10;
                default: return result;
            }
        }
        return result;
    }

    private static function isBetween(from: String, between: String, to: String) : Int {
        if(from <= between && between <= to){
            return between.charCodeAt(0) - from.charCodeAt(0);
        }
        return -1;
    }
}
